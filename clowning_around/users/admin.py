from django.contrib import admin
from clowning_around.users.models import User,Client, Troupe, TroupeLeader, Clown

admin.site.register(User)
admin.site.register(Client)
admin.site.register(Troupe)
admin.site.register(TroupeLeader)
admin.site.register(Clown)


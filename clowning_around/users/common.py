from clowning_around.users.models import User, Client, Troupe

def check_user(name):
  try:
    user = User.objects.get(username=name)
    return (user,False)
  except:
    return (None,True)


def check_email(email):
  try:
    client = Client.objects.get(contact_email=email)
    return False
  except:
    return True

def check_troupe(name):
  try:
    troupe = Troupe.objects.get(name=name)
    print(troupe)
    return troupe
  except:
    return None

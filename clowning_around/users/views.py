from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from clowning_around.users.serializers import RegisterUserSerializer,ClientSerializer
from clowning_around.users.common import check_user,check_email
from clowning_around.users.models import User, Client
from rest_framework.permissions import AllowAny


class CustomUserCreate(APIView):
    # TODO: write custom permission so that only certain category can register a user
    permission_classes = [AllowAny]

    def post(self, request,*args, **kwargs):
        try: 
            serializer = RegisterUserSerializer(data=request.data)
            if serializer.is_valid():
                name = request.data.pop('name')
                _user,_name = check_user(name)

                # check if username exist already
                if not _name:
                    data = {
                    'success':False,
                    'message': f'User with the username of: {name} already exists',
                    'data': request.data
                    }
                    return Response(data=data,status=status.HTTP_400_BAD_REQUEST)

                user_types = [value for item, value in request.data.items()] 
                if user_types.count(True) >= 2 or user_types.count(False) >= 3:
                    data = {
                    'success':False,
                    'message': 'Wrong User type opion provided: True >= 2 or False >= 3',
                    'data': request.data
                    }
                    return Response(data=data,status=status.HTTP_400_BAD_REQUEST)

                newuser =  User.objects.create(username=serializer.data.get('name'))
                newuser.name = serializer.data.get('name')
                newuser.is_troupe_leader = serializer.data.get('is_troupe_leader')
                newuser.is_clown = serializer.data.get('is_clown')
                newuser.is_client = serializer.data.get('is_client')
                newuser.save()

                data = {
                    'success':True,
                    'message': 'New user created',
                    'data': serializer.data
                }
                return Response(data=data, status=status.HTTP_201_CREATED)   
        
        except:
            data = {
                    'success':False,
                    'message': 'Invalid data',
                    'data': request.data
            }
            return Response(data=data,status=status.HTTP_400_BAD_REQUEST)



class ClientCreate(APIView):
    # TODO: write custom permission so that only certain category can register a user
    permission_classes = [AllowAny]

    def post(self, request,*args, **kwargs):

        try: 
            serializer = ClientSerializer(data=request.data)
            if serializer.is_valid():
                email = serializer.data.get('contact_email')

                print('Email1: ', email)
                if not check_email(email):
                    print('Email2: ', email)
                    data = {
                    'success':False,
                    'message': f'Client with the email address of: {email} already exists',
                    'data': request.data
                    }
                    return Response(data=data,status=status.HTTP_400_BAD_REQUEST)
                # Get the User
                _user,_name = check_user(serializer.data.get('contact_name'))
                if not _name:
                    client = Client.objects.create(
                        user=_user,
                        contact_name=serializer.data.get('contact_name'),
                        contact_email=serializer.data.get('contact_email'),
                        contact_number=serializer.data.get('contact_number'),
                    )

                    data = {
                        'success':True,
                        'message': 'New client created',
                        'data': request.data
                    }
                    return Response(data=data, status=status.HTTP_201_CREATED) 

                contact_name=serializer.data.get('contact_name')
                data = {
                    'success':False,
                    'message': f'No client with the username of : {contact_name} found',
                    'data': request.data
                }
                return Response(data=data,status=status.HTTP_400_BAD_REQUEST)
      
        except:
            data = {
                    'success':False,
                    'message': 'Invalid data',
                    'data': request.data
            }
            return Response(data=data,status=status.HTTP_400_BAD_REQUEST)































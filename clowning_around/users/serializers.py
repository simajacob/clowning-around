from rest_framework import serializers
from clowning_around.users.models import User, Client

class RegisterUserSerializer(serializers.ModelSerializer):

  class Meta:
    model = User
    fields = ('is_troupe_leader', 'is_clown', 'is_client','name')



class ClientSerializer(serializers.ModelSerializer):

  class Meta:
    model = Client
    fields = ('contact_name', 'contact_email', 'contact_number')
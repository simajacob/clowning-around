from django.urls import path
from clowning_around.users.views import (
    CustomUserCreate,
    ClientCreate
)

app_name = "users"


urlpatterns = [
    path('register/', CustomUserCreate.as_view(), name='create-user'),
    path('register/client', ClientCreate.as_view(), name='create-client'),
]

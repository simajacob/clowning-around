from django.urls import path
from clowning_around.appointments.views import CreateAppointmentView

app_name = "appointments"


urlpatterns = [
  path('create/', CreateAppointmentView.as_view(), name='create-apt'),
]

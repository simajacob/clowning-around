from django.db import models

from clowning_around.users.models import Client, Clown


class Appointment(models.Model):

    RATING_CHOICES = [
        (1, "🤡"),
        (2, "🤡🤡"),
        (3, "🤡🤡🤡"),
        (4, "🤡🤡🤡🤡"),
        (5, "🤡🤡🤡🤡🤡"),
    ]

    STATUS_CHOICES = [
        ("UPC" ,"upcoming"),
        ("IPT", "incipient"),
        ("CPT", "completed"),
        ("CCD", "canceled"),
    ]

    appointment_date = models.DateTimeField(null=True, blank=True)
    rating = models.PositiveSmallIntegerField(
        null=True, blank=True, choices=RATING_CHOICES
    )
    status = models.CharField(max_length=3, choices=STATUS_CHOICES)
    description = models.TextField()
    client = models.ForeignKey(
        to=Client,
        on_delete="SET_NULL",
        related_name="appointments",
        null=True,
        blank=True,
    )
    clown = models.ForeignKey(
        to=Clown,
        on_delete="SET_NULL",
        related_name="appointments",
        null=True,
        blank=True,
    )

    def __str__(self):
        return f"Client {self.client} at {self.appointment_date} by clown {self.clown}"


# class Issue(models.Model):
#     title = models.CharField(max_length=50)
#     description = models.TextField()
#     appointment = models.ForeignKey(
#         to=Appointment, on_delete="CASCADE", related_name="issues"
#     )

#     def __str__(self):
#         return f"{self.title} for {self.appointment}"


# class ContactRequest(models.Model):
#     for_client = models.ForeignKey(
#         to=Client, on_delete="CASCADE", related_name="contact_requests"
#     )
#     by_clown = models.ForeignKey(
#         to=Clown, on_delete="CASCADE", related_name="contact_requests"
#     )
#     reason = models.TextField()

#     def __str__(self):
#         return f"For client {self.for_client} by clown {self.by_clown}"

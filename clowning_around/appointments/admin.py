from django.contrib import admin
from clowning_around.appointments.models import Appointment

admin.site.register(Appointment)

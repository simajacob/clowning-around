from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from clowning_around.appointments.serializers import AppointmentSerializer
# from clowning_around.users.common import check_user,check_email
from clowning_around.appointments.models import Appointment
from clowning_around.users.models import Client, Troupe, User, TroupeLeader, Clown
from clowning_around.users.common import check_user, check_troupe
from rest_framework.permissions import IsAuthenticated
import datetime


#***** JSON Formt SEND
# {
#     "clown_name" : "jacob",
#     "clown_rank" : 50,
#     "troupe_name" : "Positive",
#     "troupe_max_capacity" : 30,
#     "description_apt": "This is a description of Apt",
#     "apt_date" : "2022-05-08" 
    
# }
class CreateAppointmentView(APIView):

  def post(self, request, *args, **kwargs):
    permission_classes = [IsAuthenticated]

    try: 

      if request.user.is_troupe_leader:
        clown_name = request.data.pop('clown_name')
        clown_rank = request.data.pop('clown_rank')
        troupe_name = request.data.pop('troupe_name')
        troupe_max_capacity = request.data.pop('troupe_max_capacity')
        description = request.data.pop('description_apt')
        apt_date = request.data.pop('apt_date')
        
        assigned_clown = ''
      
      # Assign a Clown
        _user, _name = check_user(clown_name )
        if _user:
          if _user.is_clown:

            troupe = check_troupe(troupe_name)
            if troupe :
              assigned_clown = Clown.objects.create(
                user = _user,
                rank = clown_rank,
                troupe = troupe
              )
    

              # Assign a Appointment
              new_appointment = Appointment.objects.create(
                  clown=assigned_clown,
                  status="IPT",
                  description=description,
                  appointment_date = datetime.datetime.strptime(apt_date, '%Y-%m-%d')
                )

              data = {
              'success':True,
              'message': f'Troupe created, Appointment in Incipient state!',
              }
              return Response(data=data,status=status.HTTP_400_BAD_REQUEST)


            data = {
                'success':False,
                'message': f'No troupe found with name of: {troupe_name}',
                }
            return Response(data=data,status=status.HTTP_400_BAD_REQUEST)


          data = {
              'success':False,
              'message': f'User has not type of clown',
              }
          return Response(data=data,status=status.HTTP_400_BAD_REQUEST)


        data = {
              'success':False,
              'message': f'No user found with the username of: {clown_name }', 
              }
        return Response(data=data,status=status.HTTP_400_BAD_REQUEST)
      

      data = {
          'success':False,
          'message': f'Access Forbidden, Only User with troupe leader privilege can access',
          }
      return Response(data=data)
    
    except:
      return Response(data= request.data, status=status.HTTP_400_BAD_REQUEST)




     